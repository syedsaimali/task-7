package com.example.activity_7;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class DisplayScreen extends AppCompatActivity {

    private GpsTracker gpsTracker;
    TextView textView1;
    TextView textView2;
    TextView textView5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gps);
        gpsTracker = new GpsTracker(DisplayScreen.this);
        if(gpsTracker.canGetLocation()){
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();
            Intent intent = getIntent();
            textView5 = findViewById(R.id.textView5);
            textView5.setText(intent.getStringExtra("date"));
            textView1 = findViewById(R.id.textView2);
            textView1.setText(String.format("%.2f", latitude));
            textView2 = findViewById(R.id.textView3);
            textView2.setText(String.format("%.2f", longitude));
        }
        else{
            gpsTracker.showSettingsAlert();
        }
    }

}