package com.example.activity_7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static String PREFS_NAME="MY_PREFERENCE";
    public static String PREF_USERNAME="username";
    public static String PREF_PASSWORD="password";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView text = (TextView) findViewById(R.id.textView4);
        SharedPreferences pref = getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        String username = pref.getString(PREF_USERNAME, null);
        text.setText("Welcome, " +username);
    }
    public void imageButtonPress(View view) {
        DatePicker date = (DatePicker) findViewById(R.id.simpleDatePicker);

        Intent intent = new Intent(this, DisplayScreen.class);
        intent.putExtra("date", (date.getYear() + "-" + date.getMonth() + "-" + date.getDayOfMonth()) );
        startActivity(intent);
    }
}