package com.example.activity_7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    public static String PREFS_NAME="MY_PREFERENCE";
    public static String PREF_USERNAME="username";
    public static String PREF_PASSWORD="password";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login); 
    }
    @Override
    protected void onStart(){
        super.onStart();
        getUser();
    }

    public void getUser(){
        SharedPreferences pref = getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        String username = pref.getString(PREF_USERNAME, null);
        String password = pref.getString(PREF_PASSWORD, null);

        if (username != null || password != null) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    public void login(View view){
        EditText txtuser=(EditText)findViewById(R.id.editTextTextPersonName);
        EditText txtpwd=(EditText)findViewById(R.id.editTextTextPassword);
        String username="John";
        String password="pass";
        if(txtuser.getText().toString().equals(username) && txtpwd.getText().toString().equals(password)){
            CheckBox ch=(CheckBox)findViewById(R.id.checkBox);
            if(ch.isChecked())
                rememberMe(username,password); //save username and password
        }
        else{
            Toast.makeText(this, "Invalid username or password",Toast.LENGTH_LONG).show();
        }


    }

    public void rememberMe(String user, String password){
        //save username and password in SharedPreferences
        getSharedPreferences(PREFS_NAME,MODE_PRIVATE)
                .edit()
                .putString(PREF_USERNAME,user)
                .putString(PREF_PASSWORD,password)
                .commit();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}